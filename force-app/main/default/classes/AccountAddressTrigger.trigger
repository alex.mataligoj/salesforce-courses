/**
 * Created by Alexandra on 16.02.2022.
 */

trigger AccountAddressTrigger on Account (before insert, before update) {
    for (Account account:Trigger.new) {
        if (account.Match_Billing_Address__c == true) {
            account.ShippingPostalCode = account.BillingPostalCode;
        }
    }
}