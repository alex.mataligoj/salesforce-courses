/**
 * Created by Alexandra on 16.02.2022.
 */

trigger ClosedOpportunityTrigger on Opportunity (after insert, after update) {
    List<Task> tasklist = new List<Task>();

    for (Opportunity opportunity: Trigger.new){
        if (opportunity.StageName == 'Closed Won'){
            tasklist.add(new Task(Subject = 'Follow Up Test Task', WhatId = opportunity.Id));
        }
    }

    if(tasklist.size()>0){
        insert tasklist;
    }
}