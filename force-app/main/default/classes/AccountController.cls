public with sharing class AccountController {

    @InvocableMethod(Label='Account Test Task')
    public static void CreateAndLinkTask(List<FlowParams> flowParamsList) {
        List<Task> listTaskUpdate = new List<Task>();
        User randomActiveUser = [SELECT Id, IsActive FROM User WHERE IsActive = TRUE LIMIT 1];

        for (FlowParams flowParam : flowParamsList) {
            if (flowParam.isNew) {
                Task task = new Task(
                        WhatId = flowParam.account.Id,
                        Subject = 'Negotiations with ' + flowParam.account.Name
                );
                listTaskUpdate.add(task);
            } else {
                if (flowParam.relatedTasks.size() > 3) {
                    for (Task task : flowParam.relatedTasks) {
                        task.OwnerId = randomActiveUser.Id;
                        listTaskUpdate.add(task);
                    }
                }
            }
        }

        if (!listTaskUpdate.isEmpty()) {
            upsert listTaskUpdate;
        }
    }

    public class FlowParams {
        @InvocableVariable(Label='Account')
        public Account account;
        @InvocableVariable(Label='isNew')
        public Boolean isNew;
        @InvocableVariable(Label='Related Task List')
        public List<Task> relatedTasks;
    }
}