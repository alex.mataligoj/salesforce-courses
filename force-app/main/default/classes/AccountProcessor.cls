/**
 * Created by Alexandra on 21.02.2022.
 */

public with sharing class AccountProcessor {
    @Future
    public static void countContacts(List<Id> accountIds) {
        List<Account> accList = [
                SELECT Id, Number_Of_Contacts__c, (SELECT Id FROM Contacts)
                FROM Account
                WHERE Id IN :accountIds
        ];
    }
}