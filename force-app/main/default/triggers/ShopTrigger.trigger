trigger ShopTrigger on Shop__c (before insert, before update, after insert, after update) {
    System.debug(' Is before insert == ' + (Trigger.isBefore && Trigger.isInsert));
    System.debug(' Is before update == ' + (Trigger.isBefore && Trigger.isUpdate));
    System.debug(' Is after insert == ' + (Trigger.isAfter && Trigger.isInsert));
    System.debug(' Is after update == ' + (Trigger.isAfter && Trigger.isUpdate));
    if (ShopTriggerHandler.enableTrigger) {
        // Do something
        System.debug('Do something');
        ShopTriggerHandler.enableTrigger = false;
    }
}